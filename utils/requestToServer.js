import { baseUrl, devUrl } from 'devConfig'

export function getReservationDistanceLimit() {
  wx.request({
    url: devUrl + 'mini/reservation/distance/limit',
    method: 'GET',
    success: res => {
      if (res.statusCode === 200) wx.setStorageSync('distanceLimit', Number(res.data.value))
    }
  })
}