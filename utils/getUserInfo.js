import { baseUrl, devUrl } from 'devConfig'

export function getUserInfo () {
  let userInfo = wx.getStorageSync('userinfo')
  if (!userInfo) {
    wx.getUserInfo({
      success: userinfo => {
        wx.setStorageSync('userinfo', userinfo.userInfo)
      },
      fail: res => {
        console.warn(res)
      }
    })
  }
}

export function getUnionid () {
  let unionid = wx.getStorageSync('unionid')
  if (unionid) {
    return unionid
  } else {
    wx.login({
      success: res => {
        wx.request({
          url: baseUrl + 'onLogin',
          data: {
            code: res.code
          },
          success: res => {
            if(res.data) {
              wx.setStorageSync('unionid', res.data)
              return res.data
            }
          },
          fail: res => {
            console.warn(res)
          }
        })
      }
    })
  }
  wx.getStorage({
    key: 'unionid',
    fail: res => {
      wx.login({
        success: res => {
          wx.request({
            url: baseUrl + 'onLogin',
            data: {
              code: res.code
            },
            success: res => {
              if (res.data) {
                wx.setStorage({
                  key: 'unionid',
                  data: res.data
                })
              }
            }
          })
        }
      })
    }
  })
}