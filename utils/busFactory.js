export default class Bus {
  constructor(index, iconId, vid, lat, lon) {
    this.iconPath = `/images/bus${iconId}.png`
    this.id = 15 + index
    this.vid = vid
    this.latitude = lat
    this.longitude = lon
    this.width = 60
    this.height = 47
  }
}