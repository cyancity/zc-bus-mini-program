// home.js
import { baseUrl, devUrl } from '../../utils/devConfig'

Page({
  data: {
    picScale: 1,
    Timer:3
  },
  onLoad: (options) => {
  },
  onReady: function () {
    var picScale=0
    var timer=0
    wx.request({
      url: baseUrl,
      method: 'POST',
      data: {
        method: 'advertisement',
      },
      header: {
        'Accept': 'application/json'
      },
      success: res => {
        if (res.statusCode == 200) {
          var json = res.data
          if (json.status == 200) {
            // json.data = json.data.excerpt.replace(/<[^>].*?>/g, "");
            this.setData({ advertisement: json.data });
          }
        }
      }
    })

    var interval = setInterval(() => {
      if(picScale<1.0){
        picScale+=0.01
      } 
      if(picScale>=1.0){
        picScale=1.0
        timer+=0.01
      }
      if(timer>1){
        clearInterval(interval)
        wx.switchTab({
          url: '/pages/map/map',
        })
      }
      this.setData({
        picScale: picScale
      })
    },15)
    this.setData({'interval': interval})
  },
  /**
   * bindViewTap event for user to enter map page and not to wait for the ad  
   */
  btnEnter: function () {
    clearInterval(this.data.interval)
    wx.switchTab({
      url: '/pages/map/map',
    })
  }
})