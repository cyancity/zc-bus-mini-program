// pages/bus_reservation/bus_reservation.js
import { stationData } from '../../resources/stations'
import getReservationDistanceLimit from '../../utils/requestToServer'
import { reservationRoutes, getReservationRoute, routeClass } from '../../resources/reservationRoute'
import { baseUrl, devUrl, websocketUrl } from '../../utils/devConfig'
import { getDistance } from '../../utils/util.js'
let app = getApp()
Page({
  data: {
    percent: 0,
    distanceLimit: 200, // maximum distance from user to station
    payloadLimit: 10, // defalut bus payload
    registered: false, // user registration status
    reservationStatus: false, // decide show reservation list in wxml or not
    reservationInfo: [], // reservation detail data
    reserving: {
      'status': false,
      'data': null
    },
    timeRange: [
      {'start': '8:10', 'end': '9:40'},
      {'start': '10:10', 'end': '11:50'},
      {'start': '14:00', 'end': '15:40'},
      {'start': '16:00', 'end': '17:40'},
      {'start': '19:00', 'end': '20:50'},
      {'start': '21:00', 'end': '22:00'}
    ]
  },


  onShow () {
    new Promise(resolve => {
      wx.request({
        url: baseUrl + 'reservation/range',
        success: res => {
          if (res.statusCode === 200 && res.data) {
            this.setData({'timeRange': res.data})
          }
          resolve()
        }
      })
    })
    .then(() => {
      if (this.canIReserve()) {
        this.setData({'reservationStatus': true})
        this.checkUserRegistration()
        .then(() => {
          wx.connectSocket({
            url: websocketUrl,
            fail: () => {
              wx.showToast({
                title: '服务器异常',
                icon: 'none'
              })
            }
          })
          wx.onSocketOpen(() => {
            let socketData = JSON.stringify({
              'method': 'getReservation'
            })
            wx.sendSocketMessage({
              data: socketData
            })
          })
        })
        .then(() => {
          this.executeSocketTask()
          wx.getStorage({
            key: 'distanceLimit',
            success: res => {
              if (!res.data && !(typeof res.data === 'number')) {
                getReservationDistanceLimit()
              } else {
                this.setData({'distanceLimit': res.data})
              }
            }
          })
          this.updateCurrentLocation()
          if (this.data.reservationStatus && this.data.registered) {
            wx.showLoading({
              'title': '加载中'
            })
            this.setPayloadLimit()
          }
        })
      } else {
        wx.showToast({
          title: '当前不是运营时间',
          icon: 'none',
          duration: 2000
        })
        this.setData({'reservationStatus': false})
      }
    })

    wx.getStorage({
      key: 'unionid',
      success: res => {
        wx.request({
          url: baseUrl + 'reservation/user/status',
          data: {'unionid': res.data},
          method: 'GET',
          success: res => {
            if (res.statusCode === 200) {
              this.setData({'reserving': res.data})
            }
          }
        })
      }
    })
  },

  executeSocketTask () {
    wx.onSocketMessage(res => {
      let socketData = JSON.parse(res.data)
      let reservation = this.data.reservationInfo || []

      switch (socketData.method) {
        case 'getReservation':
          wx.hideLoading()
          this.setReservationInfo(socketData)
          break
        case 'createReservation':
        case 'joinReservation':
          this.setUserReservingInfo(socketData, true)
          this.setReservationInfo(socketData)
          break
        case 'updateReservation':
          this.setUserReservingInfo(socketData, true)
          this.setReservationInfo(socketData)
          break
        case 'reservationFinished':
          this.setUserReservingInfo({}, false)
          this.setReservationInfo(socketData)
          this.reservationFinish()
          break
        case 'reservationError':
          wx.showToast({
            title: socketData.data.message,
            icon: 'none'
          })
          break;
        case 'quitReservation':
          this.setUserReservingInfo(socketData, false)
          this.setReservationInfo(socketData)
          break
        default:
          break;
      }
    })
  },

  setUserReservingInfo (socketData, status) {
    let reserving = {
      'status': status,
      'data': {
        'id': socketData.data.id,
        'unionid': app.globalData.unionid
      }
    }
    this.setData({'reserving': reserving})
  },

  reservationFinish () {
    wx.showModal({
      title: '预约成功',
      content: '公交车马上就到哦，请耐心等待',
      showCancel: false
    })
  },

  checkUserRegistration () {
    return new Promise((resolve, reject) => {
      wx.getStorage({
        key: 'unionid',
        success: res => {
          this.setData({'unionid': res.data})
          resolve(res.data)
        },
        fail: res => {
          console.warn(res)
        }
      })
    }).then(unionid => {
      wx.request({
        url: baseUrl + 'user/registration',
        method: 'GET',
        data: { 'unionid': unionid },
        success: res => {
          if (res.statusCode === 200) {
            wx.setStorage({
              key: 'registered',
              data: true,
              success: res => {
                this.setData({'registered': true})
                app.globalData.unionid = unionid
              }
            })
          } else {
            wx.showModal({
              title: '用户注册',
              content: '您尚未完善用户信息，注册后方可叫车',
              confirmText: '去注册',
              confirmColor: '#3CC51F',
              success: res => {
                if (res.confirm) {
                  wx.navigateTo({
                    url: '/pages/newsDetail/newsDetail?id=1073',
                  })
                } else if (res.cancel) {
                  wx.navigateBack()
                }
              }
            })
          }
        },
        fail: res => {
          wx.showToast({
            title: ' 查询注册状态失败',
            icon: 'none',
            duration: 3000
          })
        }
      })
    })
    .catch(res => {
      console.warn(res)
    })
  },

  canIReserve() {
    let range = this.data.timeRange
    let begin = new Date()
    let end = new Date()
    let now = new Date()
    let night = new Date()
    night.setHours('18', '30')
    let nightTime = (now.getTime() - night.getTime()) > 0

    this.setData({'isNight': nightTime})

    return range.some(element => {
      let startTime = element.start.split(':')
      let endTime = element.end.split(':')
      begin.setHours(startTime[0], startTime[1])
      end.setHours(endTime[0], endTime[1])
      return (now.getTime() - begin.getTime() > 0 && now.getTime() - end.getTime() < 0)
    })
  },

  setReservationInfo (socketData) {
    let defaultReservation
    if (this.data.reservationInfo.length === 0) {
      if (!this.data.isNight) {
        defaultReservation  = getReservationRoute('day')
        this.setData({'time': 'day'})
      } else {
        defaultReservation = getReservationRoute('night')
        this.setData({'time': 'night'})
      }
    } else {
      defaultReservation = this.data.reservationInfo
    }
    if (socketData.data == 'NotFound') {
      this.setData({'reservationInfo': defaultReservation})
      return
    } else {
      // handle getReservation condition, it returns an array contains four element
      if (Array.isArray(socketData.data)) {
        for (let i = 0; i < socketData.data.length; i++) {
          let index = defaultReservation.findIndex(element => element.info.name === socketData.data[i].name)
          if (index >= 0) {
            let path = `defaultReservation[${index}].info`
            let data = socketData.data[i]
            defaultReservation[index].info = socketData.data[i]
            this.setData({'reservationInfo': defaultReservation})
          }
        }
      } else {
        // update the specific reservation
        let index = defaultReservation.findIndex(element => element.info.name === socketData.data.name)
        let path = `defaultReservation[${index}].info`
        let data = socketData.data
        defaultReservation[index].info = socketData.data
        this.setData({'reservationInfo': defaultReservation})
      }
    }
  },

  /**
   * return default reservation info for per reservation
   * if status is null, and user press it, it will create a new reservation
   */
  handleReservation (event) {
    let name = event.currentTarget.dataset.name
    let id = event.currentTarget.dataset.id
    // judge status, whether the user can create or get on by his location to Station
    this.getClosestStationToRoute(name)
    .then(res => {
      // if user action is valid
      if (res.length !== 0 && !this.data.reserving.status) {
        // judge create or get reservation
        let index = this.data.reservationInfo.findIndex(element => element.info.name === name)
        let status = this.data.reservationInfo[index].info.status
        if (status === 'null') {
          this.createBusReservation(res, name)
        } else if (status === 1 || status === 3) {
          wx.showToast({
            title: '此订单已完成，请刷新后重试',
            icon: 'none',
            duration: 3000
          })
        } else {
          this.getOn(res, name, id)
        }
      } else {
        if (this.data.reserving.status) {
          wx.showToast({
            title: '请不要重复预约',
            icon: 'none'
          })
        } else {
          wx.showToast({
            title: '你离附近站点很远哦',
            icon: 'none'
          })
        }
      }
    })
  },

  createBusReservation(location, name) {
    wx.showModal({
      title: '车辆预约',
      content: '是否加入该预约？',
      success: res => {
        if (res.confirm) {
          wx.getStorage({
            key: 'unionid',
            success: unionid => {
              let socketData = {
                'method': 'createReservation',
                'data': JSON.stringify({
                  'name': name,
                  'unionid': unionid.data,
                  'location': location[0].name
                })
              }
              wx.sendSocketMessage({
                data: JSON.stringify(socketData),
                fail: () => {
                  wx.showToast({
                    title: '加载失败, 430',
                    icon: 'none'
                  })
                }
              })
            },
            fail: () => {
              wx.showToast({
                title: '加载失败, 431',
                icon: 'none'
              })
            }
          })
        }
      }
    })

  },

  /**
   * judge user location
   * if user close to the target station, show Modal to get On
   * if not show toast to tell user select correct reservation
   */
  getOn (location, name, id) {
    this.getClosestStationToRoute(name).then(res => {
      if(res) {
        wx.showModal({
          title: '车辆预约',
          content: '是否加入该预约？',
          success: res => {
            new Promise((resolve, reject) => {
              if(res.confirm) {
                wx.getStorage({
                  key: 'unionid',
                  success: function(res){
                    resolve(res)
                  }
                })
              }
            })
            .then(unionid => {
              let socketData = {
                'method': 'joinReservation',
                'data': JSON.stringify({
                  'id': id,
                  'name': name,
                  'unionid': unionid.data,
                  'location': location[0].name
                })
              }
              wx.sendSocketMessage({
                data: JSON.stringify(socketData)
              })
            })
          },
        })
      } else {
        wx.showToast({
          title: '你离附近站点很远哦',
          icon: 'none',
          duration: 2000
        })
      }
    })
  },

  getOff(event) {
    let socketData = {
      'method': 'quitReservation',
      'data': JSON.stringify(this.data.reserving.data)
    }
    wx.sendSocketMessage({
      data: JSON.stringify(socketData),
      fail: () => {
        wx.showToast({
          title: '加载失败, 432',
          icon: 'none'
        })
      }
    })
  },

  getRouteDetail (event) {
    wx.navigateTo({
      url: `/pages/stepDetail/stepDetail?name=${event.currentTarget.dataset.name}&time=${this.data.time}`
    })
  },

  /**
   * get user current location and check user distance to the station
   */
  getClosestStationToRoute (name) {
    let limit = this.data.distanceLimit
    return new Promise(resolve => {
      this.updateCurrentLocation().then(res => {
        if(res) {
          wx.getStorage({
            key: 'currentLocation',
            success: res => {
              let closestOne = reservationRoutes[routeClass[name]].filter(element => {
                let distance = getDistance(
                  res.data.latitude,
                  res.data.longitude,
                  element.latitude,
                  element.longitude
                )
                return distance < limit
              })
              resolve(closestOne.slice(0, 1))
            }
          })
        }
      })
    })
  },

  /**
   * store user location as an object to localStorage
   * if you want to get the latest user location, call this function at will
   * if you just want to get location, get it from localstorage
   */
  updateCurrentLocation () {
    return new Promise((resolve, reject) => {
      wx.getLocation({
        type: 'gcj02',
        success: res => {
          wx.setStorage({
            key: 'currentLocation',
            data: res,
            success: function(res){
              resolve(true)
            }
          })
        },
        fail: res => {
          wx.showToast({
            title: '获取位置信息失败',
            icon: 'none',
            duration: 2000
          })
          resolve(false)
        }
      })
    })
  },

  setPayloadLimit() {
    wx.request({
      url: baseUrl + 'mini/reservation/payload/limit',
      success: res => {
        if (!!res.data && res.statusCode === 200) {
          this.setData({'payloadLimit': res.data})
        } else {
          this.setData({'payloadLimit': this.data.payloadLimit})
        }
      }
    })
  },

  routeGuide() {
    wx.navigateTo({
      url: '/pages/newsDetail/newsDetail?id=1072'
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide () {
    wx.closeSocket({
      code: 1024, // on hide close socket code
      reason: 'onHideCloseSocket'
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh () {
    if (this.data.reservationStatus && this.data.registered) {
      wx.showLoading({
        'title': '加载中'
      })
      this.setPayloadLimit()

      let socketData = JSON.stringify({
        'method': 'getReservation'
      })
      wx.sendSocketMessage({
        data: socketData
      })
    }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom () {
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage () {
  }
})
