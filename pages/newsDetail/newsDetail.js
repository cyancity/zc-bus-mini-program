// news_detail.js
var WxParse = require('../../utils/wxParse/wxParse.js');
Page({
  data: {
    height: 20,
    focus: false,
    comment: "",
  },
  onLoad: function (options) {
    wx.request({
      url: 'https://zcwltech.cn/api',
      method: 'post',
      data: {
        id: options.id,
        method: 'getNews',
      },
      header: {
        'Accept': 'application/json'
      },
      success: res => {
        if (res.statusCode == 200) {
          var json = res.data;
          WxParse.wxParse('content', 'html', json.data.content, this, 25)
          this.setData({
            agree:json.data.agree,
            amusing:json.data.amusing,
          })
        } else {
        }
      }
    }),
    wx.request({
      url: 'https://zcwltech.cn/api',
      method: 'post',
      data: {
        id: options.id,
        method: 'getcomment',
      },
      header: {
        'Accept': 'application/json'
      },
      success: res => {
        if (res.statusCode == 200) {
          var json = res.data
          if (json.status == 200) {
            // json.data = json.data.excerpt.replace(/<[^>].*?>/g, "");
            this.setData({ getcomment: json.data });
          }
        }
      }
    })
    wx.getUserInfo({
      success: res => {
        this.setData({
          nickName: res.userInfo.nickName,
          userInfoAvatar: res.userInfo.avatarUrl,
        })
      }
    })
    wx.request({
      url: 'https://zcwltech.cn/api',
      method: 'post',
      data: {
        id: options.id,
        method: 'setAmusing',
      },
      header: {
        'Accept': 'application/json'
      },
      success: res => {
        if (res.statusCode == 200) {
          var json = res.data
          if (json.status == 200) {
            // that.setData({ amusing: json.data });
          }
        }
      }
    })

    // 获取接收到的id值
    var getId = options.id;
    // 让接收到的id值传递到data:{}里面
    this.setData({
      currentId: getId
    });
    // 读取所有的文章列表点赞缓存状态
    var cache = wx.getStorageSync('cache_key');
    // 如果缓存状态存在
    if (cache) {
      // 拿到所有缓存状态中的1个
      var currentCache = cache[getId];
      // 把拿到的缓存状态中的1个赋值给data中的collection，如果当前文章没有缓存状态，currentCache 的值就是 false，如果当前文章的缓存存在，那么 currentCache 就是有值的，有值的说明 currentCache 的值是 true
      this.setData({
        collection: currentCache
      })
    } else {
      // 如果所有的缓存状态都不存在 就让不存在的缓存存在
      var cache = {};
      // 既然所有的缓存都不存在，那么当前这个文章点赞的缓存也不存在，我们可以把当前这个文章点赞的缓存值设置为 false
      cache[getId] = false;
      // 把设置的当前文章点赞放在整体的缓存中
      wx.setStorageSync('cache_key', cache);
    }
  },
  // 点击图片的点赞事件  这里使用的是同步的方式
  toCollect: function (event) {
    // var count=0;
    // 获取所有的缓存
    var cache = wx.getStorageSync('cache_key');
    // 获取当前文章是否被点赞的缓存
    var currentCache = cache[this.data.currentId];
    // 取反，点赞的变成未点赞 未点赞的变成点赞
    currentCache = !currentCache;
    // 更新cache中的对应的1个的缓存值，使其等于当前取反的缓存值
    cache[this.data.currentId] = currentCache;
    // 重新设置缓存
    wx.setStorageSync('cache_key', cache);
    // 更新数据绑定,从而切换图片
    this.setData({
      // collection 默认的是 false
      // amusing: json.data,
      collection: currentCache,
    });
    // 交互反馈
    wx.showToast({
      title: currentCache ? '点赞' : '取消',
      icon: 'success',
      duration: 2000
    });
    wx.request({
      url: 'https://zcwltech.cn/api',
      method: 'post',
      data: {
        id: this.data.currentId,
        collection: this.data.collection,
        method: 'setAgree',
      },
      header: {
        'Accept': 'application/json'
      },
      success: res => {
        console.log(res);
        if (res.statusCode == 200) {
          var json = res.data
          if (json.status == 200) {
            this.setData({ amusing: json.data });
          }
        }
      }
    })
  },
  formSubmit: function (e) {
    if (e.detail.value.suggestion.length == 0) {
      wx.showToast({
        title: '输入不完整!',
        icon: 'loading',
        duration: 1500
      })
    }
    else {
      wx.request({
        url: 'https://zcwltech.cn/api',
        header: {
        },
        method: "POST",
        data: {
          method: 'setcomment',
          id: this.data.currentId,
          name: this.data.nickName,
          src: this.data.userInfoAvatar,
          comment: e.detail.value.suggestion,
        },
        success: res => {
          if (res.data.status == 200) {
            wx.showToast({
              title: "提交成功",
              icon: 'success',
              duration: 1000
            })
            this.setData({ sug: '' });
          }
          else {
            wx.showToast({
              title: "提交失败",
              icon: 'loading',
              duration: 1500
            })
          }
        }
      })
    }
  },
  onReady: function () {

  },
  onShow: function () {

  },
  onHide: function () {

  },
  onUnload: function () {
    wx.request({
      url: 'https://zcwltech.cn/api',
      method: 'post',
      data: {
        id: this.data.currentId,
        name: this.data.nickName,
        method: 'setReadPeople',
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
        if (res.statusCode == 200) {
          var json = res.data
          if (json.status == 200) {
            console.log(json.data);
          }
        }
      }
    })

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {

  },
  onShareAppMessage: function () {

  }
})