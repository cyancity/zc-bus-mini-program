// feedback.js
Page({
  data: {
    height: 20,
    focus: false,
    tel: '',
    sug: '',
  },
  bindButtonTap: function () {

  },
  bindTextAreaBlur: function (e) {
    // console.log(e.detail.value)
  },
  formSubmit: function (e) {
    console.log(e.detail.value.suggestion)
    let that = this;
    if (e.detail.value.suggestion.length == 0 || e.detail.value.tel.length == 0) {
      wx.showToast({
        title: '输入不完整!',
        icon: 'loading',
        duration: 1500
      })
    }
    else if (e.detail.value.tel.length != 11) {
      wx.showToast({
        title: '请输入11位手机号码!',
        icon: 'loading',
        duration: 1500
      })
    }
    else {
      wx.request({
        url: 'https://zcwltech.cn/api',
        header: {
        },
        method: "POST",
        data: {
          method: 'feedback',
          tel: e.detail.value.tel,
          content: e.detail.value.suggestion,
        },
        success: function (res) {
          if (res.data.status == 200) {
            wx.showToast({
              title: "提交成功",
              icon: 'success',
              duration: 1000
            })
            that.setData({ sug: '', tel: '' });
          } else {
            wx.showToast({
              title: res.data.info,
              icon: 'loading',
              duration: 1500
            })
          }
        }
      })
    }
  },
})
