// order.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    unit: '',
    time: '',
    amount: '',
    turnout: '',
    turnback: '',
    dest: '',
    place: '',
    pass: '',
    contact: '',
    tel: '',
    usage:'',
    orderType:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      usage: options.orderType
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  timeChanged: function (e) {
    this.setData({
      time: e.detail.value
    })
  },
  turnOutChanged: function (e) {
    this.setData({
      turnout: e.detail.value
    })
  },
  turnBackChanged: function (e) {
    this.setData({
      turnback: e.detail.value
    })
  },
  /**
   * 提交表单
   */
  formSubmit: function (e) {
    let that = this;
    console.log(e.detail.value.turnout)
    if (e.detail.value.unit.length == 0 || e.detail.value.time.length == 0 || e.detail.value.amount.length == 0 ||
      e.detail.value.turnout.length == 0 || e.detail.value.turnback.length == 0 || e.detail.value.dest.length == 0 ||
      e.detail.value.place.length == 0 || e.detail.value.contact.length == 0 || e.detail.value.tel.length == 0
      ){
      wx.showToast({
        title: '输入不完整!',
        icon: 'loading',
        duration: 1500
      })
    }
    else if (e.detail.value.tel.length != 11) {
      wx.showToast({
        title: '请输入11位手机号码!',
        icon: 'loading',
        duration: 1000
      })
    }
    else {
      wx.request({
        url: 'https://zcwltech.cn/api',
        header: {
        },
        method: "POST",
        data: {
          method: 'order',
          unit: e.detail.value.unit,
          time: e.detail.value.time,
          num: e.detail.value.amount,
          turnout: e.detail.value.turnout,
          turnback: e.detail.value.turnback,
          dest: e.detail.value.dest,
          place: e.detail.value.place,
          pass: e.detail.value.pass,
          contact: e.detail.value.contact,
          tel: e.detail.value.tel,
          usage: e.detail.value.usage
        },
        success: function (res) {
          console.log(res.data)
          if (res.data.status == 200 && res.data.status==200) {
            wx.showToast({
              title: "提交成功",
              icon: 'success',
              duration: 1500
            })
            that.setData({
              unit: '',
              time: '',
              amount: '',
              turnout: '',
              turnback: '',
              dest: '',
              place: '',
              pass: '',
              contact: '',
              tel: '',
              usage: ''
            });
          } else {
            wx.showToast({
              title: "提交失败",
              icon: 'loading',
              duration: 1500
            })
          }
        },
        fail:function(res){
          wx.showToast({
            title: '提交失败',
            icon: 'loading',
            duration: 1500
          })
        }
      })
    }
  },
})