// booking.js
import { getUserInfo, getUnionid } from '../../utils/getUserInfo'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    userInfo: {}  
  },

  /**
   * 点击事件
   */
  btnClick01: function(event){
    wx.navigateTo({
      url: '/pages/order/order?orderType=单日包车',
    })
  },

  btnClick02: function (event) {
    wx.navigateTo({
      url: '/pages/order/order?orderType=多日包车',
    })
  },

  btnClick03: function (event) {
    wx.navigateTo({
      url: '/pages/order/order?orderType=接送飞机',
    })
  },

  btnClick04: function (event) {
    wx.navigateTo({
      url: '/pages/order/order?orderType=接送火车',
    })
  },
  
  onLoad: function (options) {
  
  },
  onReady: function () {
  
  },
  onShow: function () {
  
  },
  onHide: function () {
  
  },
  onUnload: function () {
  
  },
  onPullDownRefresh: function () {
  
  },
  onReachBottom: function () {
  
  },
  onShareAppMessage: function () {
  
  }
})