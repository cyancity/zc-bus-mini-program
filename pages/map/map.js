// map.js
let stationData = require('../../resources/station')
let routeData = require('../../resources/route')
let route1_1 = require('../../resources/route1_1')
let route2Data = require('../../resources/route2')
let route2_2 = require('../../resources/route2_2')
let route3Data = require('../../resources/route3')
let repeat1 = require('../../resources/repeat1')
let repeat2 = require('../../resources/repeat2')
let repeat3 = require('../../resources/repeat3')
let utils = require('../../utils/util.js')
let md5 = require('../../utils/md5.js')
let Parser = require('../../lib/dom-parser.js')
Page({
  data: {
    centerX: 119.513000,
    centerY: 32.2005,
    scale: 17,
    markers: [],
    interval: null,
    polyline: [{
      points: routeData,
      color: "#F8BBD0", //粉色
      width: 3,
    }, {
      points: route1_1,
      color: "#F8BBD0", //粉色
      width: 3,
    }, {
      points: route2Data,
      color: "#29B6F6",  //蓝色
      width: 3,
    }, {
      points: route2_2,
      color: "#29B6F6",  //蓝色
      width: 3,
    }, {
      points: route3Data,
      color: "#FF6F00",  //橙色
      width: 3,
    }, {
      points: repeat1,
      color: "#80CBC4",  //绿色
      width: 3,
    }, {
      points: repeat2,
      color: "#80CBC4",  //绿色
      width: 3,
    }, {
      points: repeat3,
      color: "#80CBC4",  //绿色
      width: 3,
    }],
    controls: [{
      id: 1,
      iconPath: '/images/location.png',
      position: {
        left: 10,
        top: wx.getSystemInfoSync().windowHeight - 50 * 2,
        width: 40,
        height: 40
      },
      clickable: true
    }, {
      id: 2,
      iconPath: '/images/button1.png',
      position: {
        left: wx.getSystemInfoSync().windowWidth - 100,
        top: wx.getSystemInfoSync().windowHeight - 90,
        width: 100,
        height: 28
      },
      clickable: true
    }, {
      id: 3,
      iconPath: '/images/route.png',
      position: {
        left: 10,
        top: 10,
        width: 110,
        height: 66
      }
    }
  ]
  },

  onReady: function (e) {
    console.log('onReady')
    this.getStationMarkers()

  },
  onLoad: function () {
    console.log('onLoad')
    let that = this
    that.setData({
      markers: that.getStationMarkers()
    })
  },
  onShow: function () {
    console.log('onShow')
    let that = this
    this.data.interval = setInterval(function () {
      wx.request({
        url: 'https://zcwltech.cn/api',
        method: 'POST',
        data: {
          method: 'map'
        },
        success: function (res) {
          // console.log(res.data)
          let data = res.data;
          if (res.statusCode == 200 && data.status == 200) {
            let json = data.data
            let buses = stationData.concat()
            for (var i = 0; i < json.length; i++) {
              var j = json[i];
              var bus = that.createBus(i, parseFloat(j.mlat), parseFloat(j.mlng), j.vid)
              buses.push(bus)
            }
            that.setData({
              markers: buses
            })
          }
        }
      })
    }, 1500);
  },
  createBus(i, lat, lon, vid) {
    if (vid == "苏L50806") {
      let bus = {
        iconPath: "/images/bus1.png",
        id: 15 + i,
        vid: vid,
        latitude: lat,
        longitude: lon,
        width: 60,
        height: 47
      }
      return bus;
    }
    if (vid == "苏L50812") {
      let bus = {
        iconPath: "/images/bus1.png",
        id: 15 + i,
        vid: vid,
        latitude: lat,
        longitude: lon,
        width: 60,
        height: 47
      }
      return bus;
    }
    if (vid == "苏L50820") {
      let bus = {
        iconPath: "/images/bus1.png",
        id: 15 + i,
        vid: vid,
        latitude: lat,
        longitude: lon,
        width: 60,
        height: 47
      }
      return bus;
    }
    if (vid == "苏L50822") {
      let bus = {
        iconPath: "/images/bus1.png",
        id: 15 + i,
        vid: vid,
        latitude: lat,
        longitude: lon,
        width: 60,
        height: 47
      }
      return bus;
    }
    if (vid == "苏L50780") {
      let bus = {
        iconPath: "/images/bus2.png",
        id: 15 + i,
        vid: vid,
        latitude: lat,
        longitude: lon,
        width: 60,
        height: 47
      }
      return bus;
    }
    if (vid == "苏L50700") {
      let bus = {
        iconPath: "/images/bus2.png",
        id: 15 + i,
        vid: vid,
        latitude: lat,
        longitude: lon,
        width: 60,
        height: 47
      }
      return bus;
    }
    if (vid == "苏L50710") {
      let bus = {
        iconPath: "/images/bus2.png",
        id: 15 + i,
        vid: vid,
        latitude: lat,
        longitude: lon,
        width: 60,
        height: 47
      }
      return bus;
    }
    if (vid == "苏L50628") {
      let bus = {
        iconPath: "/images/bus2.png",
        id: 15 + i,
        vid: vid,
        latitude: lat,
        longitude: lon,
        width: 60,
        height: 47
      }
      return bus;
    }

  },
  regionchange(e) {
    console.log(e.type)
  },
  markertap(e) {
    console.log('点击标记！')
  },
  controltap(e) {
    let id = e.controlId
    if (e.controlId == 4) {
      wx.navigateTo({
        url: '../busReservation/busReservation'
      })
    }
    if (e.controlId == 2) {
      wx.navigateTo({
        url: '../routeDetail/routeDetail'
      })
    }
    if (e.controlId == 1) {
      wx.getLocation({
        type: 'gcj02', //返回可以用于wx.openLocation的经纬度
        success: res => {
          let latitude = res.latitude;
          let longitude = res.longitude;
          this.setData({
            centerX: longitude,
            centerY: latitude,
          })
        }
      });
    }
  },
  getStationMarkers() {
    let markers = [];
    for (let item of stationData) {
      let marker = this.createMarker(item);
      markers.push(marker)
    }
    return markers;
  },
  createMarker(point) {
    let latitude = point.latitude;
    let longitude = point.longitude;
    let marker = {
      iconPath: point.iconPath,
      id: point.id || 0,
      latitude: latitude,
      longitude: longitude,
      width: 80,
      height: 25
    };
    return marker;
  },
  onHide: function () {
    console.log('onHide')
    if (this.data.interval) {
      clearInterval(this.data.interval)
    }
  },

  onUnload: function () {

  },
})
