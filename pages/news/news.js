// news.js
let utils = require('../../utils/util.js')
Page({
  data: {
  },
  onLoad: function (options) {
    var that = this;
    wx.setNavigationBarTitle({
      title: options.title
    });
    let now = utils.formatTime(new Date())
    wx.request({
      url: 'https://zcwltech.cn/api',
      method: 'POST',
      data: {
        method: 'news',
        'type': options.type,
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
        console.log(res.data);
        if (res.statusCode == 200) {
          var json = res.data;
          that.setData({ news: json.data });
        }
      }
    })
  },
});