// pages/stepDetail/stepDetail.js
import { routeClass, getReservationRoute } from '../../resources/reservationRoute'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    detail: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let routes = getReservationRoute(options.time)
    let index = routes.findIndex(element => element.info.name === options.name)
    let detail = {}
    detail['detail'] = []
    routes[index].detail.forEach((element, idx, arr) => {
      if (idx === arr.length - 1) {
        detail['detail'].push({
          'current': true,
          'done': true,
          'text': element
        })
      } else {
        detail['detail'].push({
          'current': false,
          'done': true,
          'text': element
        })
      }
    })

    detail.name = routeClass[options.name]
    this.setData({'detail': detail})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})