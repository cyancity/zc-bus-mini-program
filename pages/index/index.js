// pages/index/index.js
let utils = require('../../utils/util');
let user = require('../../utils/getUserInfo')
var WxParse = require('../../utils/wxParse/wxParse');
import { baseUrl, devUrl } from '../../utils/devConfig'
import { getUnionid } from '../../utils/getUserInfo'

Page({
  data: {
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    swipers: [],
    news: [],
    cat: '17',
    navbar: ['综合资讯','原创视频'],
    currentTab: 0,
    userInfo: {}
  },
  onLoad: function (options) {
    wx.authorize({
      scope: 'scope.userInfo',
    })
    getUnionid()
    /**
     * 箭头函数时，这里如果声明this，则是undefined，匿名函数则是当前page
     * 如果想要使用 this.setData，生命周期函数就不能是箭头函数
     */
    let now = utils.formatTime(new Date())

    wx.request({
      url: baseUrl,
      method: 'POST',
      data: {
        method: 'banners',
      },
      header: {
        'Accept': 'application/json'
      },
      success: res => {
        if (res.statusCode == 200) {
          var json = res.data
          if (json.status == 200) {
            // json.data = json.data.excerpt.replace(/<[^>].*?>/g, "");
            this.setData({ swipers: json.data });
          }
        }
      }
    }),
    wx.request({
      url: baseUrl,
      method: 'POST',
      data: {
        method: 'index',
      },
      header: {
        'Accept': 'application/json'
      },
      success: res => {
        if (res.statusCode == 200) {
          var json = res.data
          if (json.status == 200) {
            // for (var i = 0; i < data.count; i++) {
            //    var excerpt_plain = data.posts[i].excerpt.replace(/<[^>].*?>/g, "");
            //    data.posts[i].excerpt_plain = excerpt_plain.replace(/\[[^\]].*?\]/g, "");
            //    news.push(data.posts[i]);
            //  }
            this.setData({ news: json.data });
          }
        }
      }
    })
    /**
     * get user info and store in localStorage
     */
    user.getUserInfo()
    
    wx.request({
      url: baseUrl,
      method: 'post',
      data: {
        id: options.id,
        method: 'getNews',
      },
      header: {
        'Accept': 'application/json'
      },
      success: res => {
        if (res.statusCode == 200) {
          var json = res.data;
          WxParse.wxParse('content', 'html', json.data.content, this, 25)
        }
      }
    })
  },
  onShow: function() {
  },
  navbarTap: function(e) {
    this.setData({
      currentTab: e.currentTarget.dataset.idx
    })
  },
});