module.exports = [
  {
    longitude: 119.511933,
    latitude: 32.206025
  }, {
    longitude: 119.511799,
    latitude: 32.205988
  }, {
    longitude: 119.511251,
    latitude: 32.205693
  }, {
    longitude: 119.510694,
    latitude: 32.205394
  }, {
    longitude: 119.510554,
    latitude: 32.205294
  }, {
    longitude: 119.510356,
    latitude: 32.204894
  }, {
    longitude: 119.510495,
    latitude: 32.204223
  }, {
    longitude: 119.510683,
    latitude: 32.203868
  }, {
    longitude: 119.511096,
    latitude: 32.203274
  }, {
    longitude: 119.511182,
    latitude: 32.203065
  }, {
    longitude: 119.511182,
    latitude: 32.203061
  }, {
    longitude: 119.511230,
    latitude: 32.202752
  }, {
    longitude: 119.511155,
    latitude: 32.202348
  }, {
    longitude: 119.511101,
    latitude: 32.202107
  },
]