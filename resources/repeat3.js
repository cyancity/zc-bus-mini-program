module.exports = [
  {
    longitude: 119.513381,
    latitude: 32.197554
  }, {
    longitude: 119.514170,
    latitude: 32.197318
  }, {
    longitude: 119.514910,
    latitude: 32.197300
  }, {
    longitude: 119.514878,
    latitude: 32.197227
  }, {
    longitude: 119.514765,
    latitude: 32.197182
  }, {
    longitude: 119.514728,
    latitude: 32.197128
  }, {
    longitude: 119.514722,
    latitude: 32.197069
  }, {
    longitude: 119.514722,
    latitude: 32.196061
  }, {
    longitude: 119.514771,
    latitude: 32.195507
  }, {
    longitude: 119.515200,
    latitude: 32.195439
  }, {
    longitude: 119.515854,
    latitude: 32.195325
  }, {
    longitude: 119.516315,
    latitude: 32.195298
  }, {
    longitude: 119.516884,
    latitude: 32.195235
  }, {
    longitude: 119.517367,
    latitude: 32.195167
  }, {
    longitude: 119.517635,
    latitude: 32.195153
  },{
    longitude: 119.517973,
    latitude: 32.195148
  }, {
    longitude: 119.518311,
    latitude: 32.195180
  }, {
    longitude: 119.518601,
    latitude: 32.195189
  },
]