export const reservationRoutes = {
  '一号线': [
    {
      name: "六食堂",
      latitude: 32.205869,
      longitude: 119.512269,
    }, 
    {
      name: "海外公寓",
      latitude: 32.204157,
      longitude: 119.510507,
    }, 
    {
      name: "药学院",
      latitude: 32.197862,
      longitude: 119.513384,
    }, 
    {
      name: "图书馆",
      latitude: 32.197395,
      longitude: 119.511281,
    }, 
    {
      name: "电气学院",
      latitude: 32.197493,
      longitude: 119.510485,
    }, 
    {
      name: "医学院 ",
      latitude: 32.195987,
      longitude: 119.511974,
    }, 
    {
      name: "材料学院",
      latitude: 32.196087,
      longitude: 119.514709,
    }, 
    {
      name: "五棵松",
      latitude: 32.195151,
      longitude: 119.518684,
    }, 
    {
      name: "三山楼",
      latitude: 32.195290,
      longitude: 119.516346
    }, 
    {
      name: "流体中心",
      latitude: 32.198275,
      longitude: 119.516115
    },
    {
      name: "三岔口",
      latitude: 32.198738,
      longitude: 119.515648,
    }, 
    {
      name: "C区8栋",
      latitude: 32.200944,
      longitude: 119.513926,
    }, 
    {
      name: "新一区",
      latitude: 32.203421,
      longitude: 119.515491,
    }, 
    {
      name: "D区9栋",
      latitude: 32.204793,
      longitude: 119.514559,
    }
  ],
  '二号线': [
    {
      name: "六食堂",
      latitude: 32.205869,
      longitude: 119.512269,
    },
    {
      name: "新一区",
      latitude: 32.203421,
      longitude: 119.515491,
    },
    {
      name: "D区9栋",
      latitude: 32.204793,
      longitude: 119.514559,
    },
    {
      name: "C区8栋",
      latitude: 32.200944,
      longitude: 119.513926,
    },
    {
      name: "三岔口",
      latitude: 32.198738,
      longitude: 119.515648,
    },
    {
      name: "流体中心",
      latitude: 32.198275,
      longitude: 119.516115
    },
    {
      name: "五棵松",
      latitude: 32.195151,
      longitude: 119.518684,
    },
    {
      name: "三山楼",
      latitude: 32.195290,
      longitude: 119.516346
    },
    {
      name: "材料学院",
      latitude: 32.196087,
      longitude: 119.514709,
    },
    {
      name: "医学院 ",
      latitude: 32.195987,
      longitude: 119.511974,
    },
    {
      name: "图书馆",
      latitude: 32.197395,
      longitude: 119.511281,
    },
    {
      name: "电气学院",
      latitude: 32.197493,
      longitude: 119.510485,
    },
    {
      name: "海外公寓",
      latitude: 32.204157,
      longitude: 119.510507,
    },
  ],
  '三号线': [
    {
      name: "六食堂",
      latitude: 32.205869,
      longitude: 119.512269,
    },
    {
      name: "五棵松",
      latitude: 32.195151,
      longitude: 119.518684,
    },
    {
      name: "三山楼",
      latitude: 32.195290,
      longitude: 119.516346
    },
    {
      name: "流体中心",
      latitude: 32.198275,
      longitude: 119.516115
    },
    {
      name: "三岔口",
      latitude: 32.198738,
      longitude: 119.515648,
    },
    {
      name: "C区8栋",
      latitude: 32.200944,
      longitude: 119.513926,
    },
    {
      name: "D区9栋",
      latitude: 32.204793,
      longitude: 119.514559,
    },
    {
      name: "海外公寓",
      latitude: 32.204157,
      longitude: 119.510507,
    },
  ]
}

export const routeClass = {
  'firstDepartureDayTime': '一号线',
  'firstReturnDayTime': '一号线',
  'secondDepatureDayTime': '二号线',
  'secondReturnDayTime': '二号线',
  'departureNightTime': '三号线',
  'returnNightTime': '三号线' 
}

export const reservationStations = [
  {
    name: "六食堂",
    latitude: 32.205869,
    longitude: 119.512269,
  }, 
  {
    name: "海外公寓",
    latitude: 32.204157,
    longitude: 119.510507,
  }, 
  {
    name: "药学院",
    latitude: 32.197862,
    longitude: 119.513384,
  }, 
  {
    name: "图书馆",
    latitude: 32.197395,
    longitude: 119.511281,
  }, 
  {
    name: "电气学院",
    latitude: 32.197493,
    longitude: 119.510485,
  }, 
  {
    name: "医学院 ",
    latitude: 32.195987,
    longitude: 119.511974,
  }, 
  {
    name: "材料学院",
    latitude: 32.196087,
    longitude: 119.514709,
  }, 
  {
    name: "五棵松",
    latitude: 32.195151,
    longitude: 119.518684,
  }, 
  {
    name: "三山楼",
    latitude: 32.195290,
    longitude: 119.516346
  }, 
  {
    name: "流体中心",
    latitude: 32.198275,
    longitude: 119.516115
  },
  {
    name: "三岔口",
    latitude: 32.198738,
    longitude: 119.515648,
  }, 
  {
    name: "C区8栋",
    latitude: 32.200944,
    longitude: 119.513926,
  }, 
  {
    name: "新一区",
    latitude: 32.203421,
    longitude: 119.515491,
  }, 
  {
    name: "D区9栋",
    latitude: 32.204793,
    longitude: 119.514559,
  }
]

const firstDepartureDayTime = {
  'info': {},
  'steps': [
    {
      current: false,
      done: true,
      text: '六食堂'
    },
    {
      done: true,
      current: false,
      text: '图书馆'
    },
    {
      done: true,
      current: true,
      text: '五棵松'
    }
  ],
  'detail': ['六食堂', '海外公寓', '电气学院', '图书馆', '医学院', '材料学院', '三山楼', '五棵松']
}

const firstReturnDayTime = {
  'info': {},
  'steps': [
    {
      current: false,
      done: true,
      text: '五棵松'
    },
    {
      done: true,
      current: false,
      text: '图书馆'
    },
    {
      done: true,
      current: true,
      text: '六食堂'
    }
  ],
  'detail': ['五棵松', '流体中心', '三岔口', 'C区8栋', '新一区', 'D区9栋', '六食堂']
}

const secondDepatureDayTime = {
  'info': {},
  'steps': [
    {
      current: false,
      done: true,
      text: '六食堂'
    },
    {
      done: true,
      current: false,
      text: '新一区'
    },
    {
      done: true,
      current: true,
      text: '五棵松'
    }
  ],
  'detail': ['六食堂', 'D区9栋', '新一区', 'C区8栋', '三岔口', '流体中心', '五棵松']
}

const secondReturnDayTime = {
  'info': {},
  'steps': [
    {
      current: false,
      done: true,
      text: '五棵松'
    },
    {
      done: true,
      current: false,
      text: '新一区'
    },
    {
      done: true,
      current: true,
      text: '六食堂'
    }
  ],
  'detail': ['五棵松', '三山楼', '材料学院', '医学院', '电气学院', '海外公寓', '六食堂']
}


const returnNightTime = {
  'info': {},
  'steps': [
    {
      current: false,
      done: true,
      text: '五棵松'
    },
    {
      done: true,
      current: false,
      text: '新一区'
    },
    {
      done: true,
      current: true,
      text: '六食堂'
    }
  ],
  'detail': ['五棵松', '三山楼', '流体中心', '三岔口', 'C区8栋', '新一区', 'D区9栋', '海外公寓', '六食堂']
}

const departureNightTime = {
  'info' : {},
  'steps': [
    {
      current: false,
      done: true,
      text: '六食堂'
    },
    {
      done: true,
      current: false,
      text: '新一区'
    },
    {
      done: true,
      current: true,
      text: '五棵松'
    }
  ],
  'detail': ['六食堂', '海外公寓', 'D区9栋', '新一区', 'C区8栋', '三岔口', '流体中心', '三山楼', '五棵松'] 
}

const dayRoutesName = ['firstDepartureDayTime', 'firstReturnDayTime', 'secondDepatureDayTime', 'secondReturnDayTime']
const nightRoutesName = ['departureNightTime', 'returnNightTime']

function getDefault (i, time) {
  let choice = time === 'day' ? dayRoutesName[i] : nightRoutesName[i]
  return {
    'id': i,
    'name': choice,
    'station': '',
    'created_at': '',
    'payload': 0,
    'status': 'null'
  }
}

function setDefaultReservationInfo (arr, time) {
  for (let i = 0; i < arr.length; i++) {
    arr[i]['info'] = getDefault(i, time)
  }
  return arr
}


export const getReservationRoute = function (time) {
  let arr
  if (time === 'day') {
    arr = [firstDepartureDayTime, firstReturnDayTime, secondDepatureDayTime, secondReturnDayTime]
    return setDefaultReservationInfo(arr, time)
  } else if (time === 'night') {
    arr = [departureNightTime, returnNightTime]
    return setDefaultReservationInfo(arr, time)
  }
}